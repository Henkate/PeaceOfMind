<?xml version="1.0" encoding="utf-8"?>

<!--
    Copyright (C) 2013 Mudar Noufal, PeaceOfMind+

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->

<resources>
    <string name="app_name">Peace of Mind+</string>
    <string name="activity_settings">Settings</string>
    <string name="activity_splash">@string/app_name</string>
    <string name="activity_about">About</string>
    <string name="activity_about_libs">Acknowledgements</string>
    <string name="activity_eula">EULA</string>

    <!--Menu items-->
    <string name="action_settings">Settings</string>
    <string name="action_about">About</string>
    <string name="action_share">Share</string>
    <string name="action_rate">Rate</string>
    <string name="action_eula">License</string>
    <string name="action_about_libs">Open source libraries</string>

    <!--Buttons-->
    <string name="btn_restart">Restart</string>
    <string name="btn_grant_permission">Grant permission</string>
    <string name="btn_grant">Grant</string>
    <string name="btn_got_it">Got it</string>

    <!--Timer UI-->
    <string name="title_at_peace_off">@string/app_name</string>
    <string name="title_at_peace_on">At peace</string>
    <string name="subtitle_duration">Duration</string>
    <string name="subtitle_end_time">End time</string>

    <!--Time strings-->
    <string name="duration_hours_minutes">%1$sh%2$s</string>

    <!--Do-not-disturb mode-->
    <string name="ringer_none">None</string>
    <string name="ringer_priority">Priority</string>
    <string name="dnd_total_silence">Total silence</string>
    <string name="dnd_alarms_only">Alarms only</string>
    <string name="dnd_priority_only">Priority only</string>
    <string name="airplane_mode">Airplane mode</string>

    <!--Splash-->
    <string name="splash_title">Disconnect for a&#160;moment</string>
    <string name="splash_desc_1">Our phones take&#160;up increasing space in our lives and we are constantly connected.</string>
    <string name="splash_desc_2">Would you like to disconnect from the world, if only for a&#160;moment?</string>
    <string name="splash_desc_3">You can mute your device and limit interruptions, stopping sounds or&#160;notifications to&#160;enjoy some peace of&#160;mind.</string>

    <!--Settings-->
    <string name="prefs_cat_notification">Notifications</string>
    <string name="prefs_cat_advanced">Permissions</string>
    <string name="prefs_title_duration">Maximum duration</string>
    <string name="prefs_dialog_title_duration">Set maximum duration</string>

    <string name="prefs_title_notification_listener">Notification access</string>
    <string name="prefs_summary_notification_listener_optional">Allow app to hide other apps’ notifications</string>
    <string name="prefs_summary_notification_listener_required">Required to allow app to handle system priority interruptions</string>
    <string name="prefs_summary_notification_listener_disabled">Sorry, setting not found! Please check your device settings for “Notification access” to enable manually.</string>

    <string name="prefs_title_dnd">Do not disturb access</string>
    <string name="prefs_summary_dnd">Required to allow app to handle system’s Do&#160;not&#160;disturb mode</string>
    <string name="prefs_title_battery_optimization">Battery optimization whitelist</string>
    <string name="prefs_summary_battery_optimization">Recommended for precise At&#160;peace end&#160;time</string>

    <string name="prefs_title_enable_airplane_mode">Airplane mode</string>
    <string name="prefs_summary_enable_airplane_mode">Requires root access</string>

    <string name="prefs_title_has_end_notification">Notification</string>
    <string name="prefs_summary_has_end_notification">Notify when At peace mode ends</string>
    <string name="prefs_title_notification_vibrate">Vibrate</string>
    <string name="prefs_title_notification_ringtone">Sound</string>
    <string name="prefs_summary_notification_ringtone_silent">Silent</string>
    <string name="prefs_title_notification_channel_settings">Sound and vibration</string>
    <string name="prefs_summary_notification_channel_settings">System settings</string>

    <string name="prefs_duration_fast">3 hours</string>
    <string name="prefs_duration_moderate">6 hours</string>
    <string name="prefs_duration_slow">12 hours</string>

    <!--About-->
    <string name="about_title">@string/app_name</string>
    <string name="about_subtitle">Disconnect from the world, if&#160;only for a&#160;moment</string>
    <string name="about_intro">Mute your device and limit interruptions, stopping sounds or notifications to&#160;enjoy some peace of&#160;mind.</string>
    <string name="about_fairphone_credits">Peace of&#160;Mind was originally a Fairphone app. Peace&#160;of&#160;Mind+ aims to bring&#160;it to other Android devices, along with new&#160;features.</string>
    <string name="about_source_code">Source code is released under the Apache License version 2.0.</string>
    <string name="about_dev_credits">Android app developed by Mudar&#160;Noufal.</string>
    <string name="about_version">Version %s</string>

    <!--Share-->
    <string name="share_intent_title">Checkout “Peace of Mind+” on Google Play</string>

    <!--Notification-->
    <string name="notif_start_title">At peace started</string>
    <string name="notif_start_text">Scheduled to end at %s</string>
    <string name="notif_end_title">Back to normal</string>
    <string name="notif_end_text">At peace mode ended at %s</string>
    <string name="notif_action_stop">Stop</string>
    <string name="notif_channel_name">Default</string>
    <string name="notif_channel_description">Displays current At peace duration and end&#160;time, with a Stop action button.</string>

    <!--SnackBar-->
    <string name="msg_restart_app_for_root_settings">Root detected: restart app to enable advanced settings</string>
    <string name="msg_usage_hint">Drag to select duration of At&#160;peace (Do&#160;not disturb) mode</string>
    <string name="msg_airplane_mode_enabled">Airplane mode is ON</string>
    <string name="msg_permissions_required">App needs permission to access Do&#160;not disturb mode</string>

</resources>
