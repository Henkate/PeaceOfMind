/*
 * Copyright (C) 2013 Mudar Noufal, PeaceOfMind+
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.mudar.fairphone.peaceofmind.superuser;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.containers.RootClass;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.CommandCapture;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import ca.mudar.fairphone.peaceofmind.Const;
import ca.mudar.fairphone.peaceofmind.R;
import ca.mudar.fairphone.peaceofmind.data.PeaceOfMindPrefs;

import static ca.mudar.fairphone.peaceofmind.Const.PeaceOfMindIntents;

@RootClass.Candidate
public class SuperuserHelper {
    private static final String TAG = "SuperuserHelper";
    private static final String[] COMMAND_AIRPLANE_ON = {
            "settings put global airplane_mode_on 1",
            "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true -e " + PeaceOfMindIntents.EXTRA_STATE + " true " + " -e " + PeaceOfMindIntents.EXTRA_TOGGLE + " true"
    };
    private static final String[] COMMAND_AIRPLANE_OFF = {
            "settings put global airplane_mode_on 0",
            "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false -e " + PeaceOfMindIntents.EXTRA_STATE + " false " + " -e " + PeaceOfMindIntents.EXTRA_TOGGLE + " true"
    };

    public static void initialAccessRequest(final Context context) {
        // For JellyBean, request SuperUser access
        if (Const.SUPPORTS_JELLY_BEAN_MR1) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    boolean hasAirplaneMode = PeaceOfMindPrefs.hasAirplaneMode(prefs);
                    if (hasAirplaneMode) {
                        final boolean isAccessGiven = RootTools.isAccessGiven();
                        PeaceOfMindPrefs.setAccessGiven(isAccessGiven, prefs);
                        if (!isAccessGiven) {
                            PeaceOfMindPrefs.setAirplaneMode(false, prefs);
                        }
                    }
                }
            };
            thread.start();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static void setAirplaneModeSettings(final Context context, final int value) {

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                final boolean isAccessGiven = PeaceOfMindPrefs.isAccessGiven(prefs);
                if (isAccessGiven) {
                    runShellCommands(context, value);
                } else {
                    PeaceOfMindPrefs.setAirplaneMode(false, prefs);
                }
            }
        });
    }

    private static void runShellCommands(final Context context, final int value) {
        final CommandCapture command = new CommandCapture(0, true,
                (value == 1 ? COMMAND_AIRPLANE_ON : COMMAND_AIRPLANE_OFF)) {

            @Override
            public void commandCompleted(int i, int i2) {
                if (value == 1 && context != null) {
                    Toast.makeText(context, R.string.airplane_mode_enabled, Toast.LENGTH_SHORT).show();
                }
            }
        };

        try {
            RootTools.getShell(true).add(command);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (RootDeniedException e) {
            e.printStackTrace();
        }
    }
}
