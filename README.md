## Peace of Mind+, a fork of Fairphone’s app

[![Peace of Mind+][img_app_logo]][link_app_playstore]

### Disconnect from the world, if only for a moment

We become more conscious of our phones as our lives become increasingly connected. Would you like to disconnect, if only for a moment? Mute your device and limit interruptions, stopping sounds or notifications to enjoy some peace of mind.

Originally developed by Kwame Corporation for the **Fairphone**, [Peace of Mind+][link_app_playstore] aims simply to bring this beautiful app to other devices, along with new features.

Peace of Mind+ is an open-source project released under the Apache License version 2.0.

**Disclaimer:** This is not the official App, and I’m not related to KwameCorp or the Fairphone project. Credit (and many thanks) go to Fairphone/KwameCorp!

## Features
* Support for Android’s Do not disturb mode and Priority notifications.
* Support for Airplance mode (requires root).
* Settings to set maximum duration (3, 6 or 12 hours).
* Compatible with devices running Android KitKat (4.4) or later.
* Material Design!

## Links

* [Website][link_app_website]
* [Source code on GitLab][link_app_source]
* [XDA thread][link_app_xda]
* [Privacy policy][link_app_privacy]
* [Peace of Mind+ on Google Play][link_app_playstore]

[![Android app on Google Play][img_playstore_badge]][link_app_playstore]

## Credits

* Developed by [Mudar Noufal][link_mudar_ca]  &lt;<mn@mudar.ca>&gt;
* Updated translations by [martinusbe][link_xda_martinusbe] (NL), [Benko111][link_xda_benko111] (DE), [gaich][link_xda_gaich] (RU), [mauam][link_xda_mauam] (PT), [eduds][link_xda_eduds] (PT-BR) and [ccamara][link_github_ccamara] (CA).
* Many thanks to [Kwamecorp][link_kwamecorp] and the [Fairphone][link_fairphone] team!

This Android app includes libraries and derivative work of the following projects:

* [SeekArc][link_lib_seekarc] &copy; Neil Davies.
* [Otto][link_lib_otto] &copy; Square, Inc.
* [RootShell][link_lib_rootshell] &copy; Stephen Erickson, Chris Ravenscroft.
* [AboutLibraries][link_lib_aboutlibs] &copy; Mike Penz.
* [Crashlytics][link_lib_crashlytics] &copy; Google, Inc.
* [Android Support Library v7][link_lib_supportv7] &copy; The Android Open Source Project.
* [AOSP][link_lib_aosp] &copy; The Android Open Source Project.

These five projects are all released under the [Apache License v2.0][link_apache].

## Code license

    Copyright (C) 2013 Fairphone Project
    Copyright (C) 2013 Mudar Noufal, PeaceOfMind+
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

[![Android app on Google Play][img_app_screenshots]][link_app_playstore]

[img_app_logo]: http://fairphone.mudar.ca/images/peaceofmindplus_logo_200x200.png
[img_app_screenshots]: http://fairphone.mudar.ca/images/peaceofmindplus_screenshots.png
[img_playstore_badge]: http://fairphone.mudar.ca/images/en_badge_web_generic_200px.png
[link_app_playstore]: https://play.google.com/store/apps/details?id=ca.mudar.fairphone.peaceofmind
[link_app_website]: http://fairphone.mudar.ca/
[link_app_privacy]: http://fairphone.mudar.ca/privacy.html
[link_app_xda]: https://forum.xda-developers.com/showthread.php?t=2573945
[link_app_source]: https://gitlab.com/mudar-ca/PeaceOfMind
[link_mudar_ca]: http://www.mudar.ca/
[link_kwamecorp]: https://github.com/Kwamecorp
[link_fairphone]: https://www.fairphone.com/
[link_xda_martinusbe]: https://forum.xda-developers.com/member.php?u=4139665
[link_xda_benko111]: https://forum.xda-developers.com/member.php?u=5276854
[link_xda_gaich]: https://forum.xda-developers.com/member.php?u=4563466
[link_xda_mauam]: https://forum.xda-developers.com/member.php?u=3563392
[link_xda_eduds]: https://forum.xda-developers.com/member.php?u=5161712
[link_github_ccamara]: https://github.com/ccamara
[link_lib_seekarc]: https://github.com/neild001/SeekArc
[link_lib_otto]: https://square.github.io/otto/
[link_lib_rootshell]: https://github.com/Stericson/RootShell
[link_lib_aboutlibs]: https://github.com/mikepenz/AboutLibraries
[link_lib_crashlytics]: https://fabric.io/kits/android/crashlytics
[link_lib_supportv7]: https://developer.android.com/tools/support-library/
[link_lib_aosp]: https://source.android.com/
[link_apache]: https://www.apache.org/licenses/LICENSE-2.0
