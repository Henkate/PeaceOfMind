## Title (50 chars)

Peace of Mind+

## Short description (80 chars)

Déconnectez-vous pour retrouver un moment de paix

## Full description (4000 chars)

Fork* de l’application Peace of Mind du projet éthique Fairphone.

Nos téléphones prennent de plus en plus de place dans nos vies et nous sommes connectés en permanence. Aimeriez-vous pouvoir vous déconnecter, ne serait-ce que pour quelques heures?

Vous pouvez limiter les interruptions en bloquant les sons et les notifications de votre appareil pour profiter d’un moment de tranquillité.

Si vous appareil est rooté, vous pouvez également utiliser le chronomètre pour activer le mode avion pour une période déterminée.

À l’origine, Peace of Mind est une application du Fairphone. Elle a inspiré Peace of Mind+ afin de proposer cette app aux autres appareils Android en y ajoutant de nouvelles fonctionnalités.

Peace of Mind+ est un projet libre publié sous la licence Apache v2.0. Le code source est disponible sur GitLab https://gitlab.com/mudar-ca/PeaceOfMind

Clause de non-responsabilité: ceci n’est pas l’application officielle du Fairphone et je ne suis pas affilié à KwameCorp ou Fairphone. Une grande partie du crédit revient à Fairphone/KwameCorp, avec tous mes remerciements!

(*) Open-source fork: fourche, embranchement, clone. Logiciel développé en réutilisant le code source d'un logiciel existant.
